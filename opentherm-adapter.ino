/*
OpenTherm Master Communication Example
By: Ihor Melnyk
Date: January 19th, 2018

Uses the OpenTherm library to get/set boiler status and water temperature
Open serial monitor at 115200 baud to see output.

Hardware Connections (OpenTherm Adapter (http://ihormelnyk.com/pages/OpenTherm) to Arduino/ESP8266):
-OT1/OT2 = Boiler X1/X2
-VCC = 5V or 3.3V
-GND = GND
-IN  = Arduino (3) / ESP8266 (5) Output Pin
-OUT = Arduino (2) / ESP8266 (4) Input Pin

Controller(Arduino/ESP8266) input pin should support interrupts.
Arduino digital pins usable for interrupts: Uno, Nano, Mini: 2,3; Mega: 2, 3, 18, 19, 20, 21
ESP8266: Interrupts may be attached to any GPIO pin except GPIO16,
but since GPIO6-GPIO11 are typically used to interface with the flash memory ICs on most esp8266 modules, applying interrupts to these pins are likely to cause problems
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <PubSubClient.h>
#include <OpenTherm.h>

#define NEED_SERIAL_PRINT

//OpenTherm input and output wires connected to 4 and 5 pins on the OpenTherm Shield
const int inPin = 4;
const int outPin = 5;
OpenTherm ot(inPin, outPin);

// Wifi configuration
constexpr const char * const wifiSsid = "SmartHome";
constexpr const char * const wifiPassword = "UkP*5wBR2Tg8";
constexpr uint8_t maxWifiReconnectAttempts = 100;

// Mqtt configuration
constexpr const char * const mqttServerHost = "192.168.100.10";
constexpr uint16_t mqttServerPort = 1883;
constexpr const char * const mqttClientName = "BoilerController";
constexpr uint8_t maxMqttReconnectAttempts = 5;

// Mqtt topics
constexpr const char * const topicAirTemp = "/sensor/temp/floor1";
constexpr const char * const topicBoilerTemp = "/sensor/boiler/temp";
constexpr const char * const topicBoilerHeating = "/sensor/boiler/heating";
constexpr const char * const topicBoilerWater = "/sensor/boiler/water";
constexpr const char * const topicBoilerFlame = "/sensor/boiler/flame";

ESP8266WebServer server(80);

WiFiClient wifiClient;
PubSubClient mqttClient(mqttServerHost, mqttServerPort, wifiClient);

float invalidTempValue = -100.;

float sp = 25, //set point
pv = invalidTempValue, //current temperature
pv_last = invalidTempValue, //prior temperature
ierr = 0, //integral error
dt = 0, //time between measurements
op = 0; //PID controller output
unsigned long ts = 0, new_ts = 0; //timestamp

bool isCentralHeatingActive;
bool isHotWaterEnabled;
bool isFlameOn;
float boilerTemperature;

const char HTTP_HTML[] PROGMEM = "<!doctypehtml><meta charset=\"UTF-8\"><meta name=\"viewport\"content=\"width=device-width,initial-scale=1\"><title>Boiler controller</title><script>window.setInterval(\"update()\",5e3);async function update(){const e=await fetch(\"/status\");var t=await e.json();document.getElementById(\"temp\").innerHTML=t.temp,document.getElementById(\"boiler\").innerHTML=t.boiler,document.getElementById(\"heat\").innerHTML=t.heat?\"вкл.\":\"выкл.\",document.getElementById(\"water\").innerHTML=t.water?\"вкл.\":\"выкл.\",document.getElementById(\"flame\").innerHTML=t.flame?\"вкл.\":\"выкл.\"}</script><body style=\"text-align:center\"><h1>Boiler controller</h1><font size=\"6\">Температура воздуха: <span id=\"temp\">{0}</span>&deg;</font><br><br><font size=\"5\">Температура носителя: <span id=\"boiler\">{1}</span>&deg;</font><br><br><font size=\"5\">Отопление: <span id=\"heat\">{2}</span></font><br><br><font size=\"5\">Нагрев воды: <span id=\"water\">{3}</span></font><br><br><font size=\"5\">Пламя: <span id=\"flame\">{4}</span></font><br><br><form method=\"post\"style=\"font-size:30px\">Хочу температуру: <input style=\"font-size:30px;width:80px\"name=\"sp\"value=\"{5}\"><br><br><input type=\"submit\"style=\"font-size:30px;padding:10px\"value=\"Установить\"></form>";

const char STATUS_JSON[] PROGMEM = "{\"temp\":{0},\"boiler\":{1},\"heat\":{2},\"water\":{3},\"flame\":{4}}";

void ICACHE_RAM_ATTR handleInterrupt()
{
  ot.handleInterrupt();
}

float pid(float sp, float pv, float pv_last, float& ierr, float dt)
{
	float Kc = 10.0; // K / %Heater
	float tauI = 50.0; // sec
	float tauD = 1.0;  // sec
	// PID coefficients
	float KP = Kc;
	float KI = Kc / tauI;
	float KD = Kc*tauD;	
	// upper and lower bounds on heater level
	float ophi = 100;
	float oplo = 0;
	// calculate the error
	float error = sp - pv;
	// calculate the integral error
	ierr = ierr + KI * error * dt;	
	// calculate the measurement derivative
	float dpv = (pv - pv_last) / dt;
	// calculate the PID output
	float P = KP * error; //proportional contribution
	float I = ierr; //integral contribution
	float D = -KD * dpv; //derivative contribution
	float op = P + I + D;
	// implement anti-reset windup
	if ((op < oplo) || (op > ophi)) {
		I = I - KI * error * dt;
		// clip output
		op = max(oplo, min(ophi, op));
	}
	ierr = I;	
	Serial.println("sp="+String(sp) + " pv=" + String(pv) + " dt=" + String(dt) + " op=" + String(op) + " P=" + String(P) + " I=" + String(I) + " D=" + String(D));
	return op;
}

void replaceValues(String& value)
{
  value.replace("{0}", String(pv, 1));
  value.replace("{1}", String(boilerTemperature, 1));
  value.replace("{2}", String(isCentralHeatingActive ? "true" : "false"));
  value.replace("{3}", String(isHotWaterEnabled ? "true" : "false"));
  value.replace("{4}", String(isFlameOn ? "true" : "false"));
}

void handleRoot()
{
	if (server.method() == HTTP_POST)
  {
		for (uint8_t i = 0; i<server.args(); i++)
    {
			if (server.argName(i) == "sp") 
      {
				sp = server.arg(i).toFloat();
			}			
		}
	}

	String page = FPSTR(HTTP_HTML);
  replaceValues(page);
  page.replace("{5}", String(sp, 1));
	server.send(200, "text/html", page);
}

void handleGetStatus()
{
	String status = FPSTR(STATUS_JSON);
  replaceValues(status);
  server.send(200, "application/json", status);
}

bool wifiConnect()
{
  #ifdef NEED_SERIAL_PRINT
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(wifiSsid);
  #endif

  WiFi.mode(WIFI_STA);
  WiFi.begin(wifiSsid, wifiPassword);

  uint8_t attempts = maxWifiReconnectAttempts;
  while (WiFi.status() != WL_CONNECTED && attempts > 0)
  {
    --attempts;
    delay(500);
    #ifdef NEED_SERIAL_PRINT
      Serial.print(".");
    #endif
  }

  if (WiFi.status() != WL_CONNECTED)
  {
    return false;
  }

  #ifdef NEED_SERIAL_PRINT
    Serial.println();
    Serial.print("WiFi connected.");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  #endif

  return true;
}

bool mqttConnect()
{
  uint8_t attempts = maxMqttReconnectAttempts;
  
  while (!mqttClient.connected() && attempts > 0)
  {
    --attempts;

    #ifdef NEED_SERIAL_PRINT
      Serial.print("Attempting MQTT connection...");
    #endif
    
    if (mqttClient.connect(mqttClientName))
    {
      #ifdef NEED_SERIAL_PRINT
        Serial.println("connected");
      #endif
    }
    else
    {
      #ifdef NEED_SERIAL_PRINT
        Serial.print("failed, rc=");
        Serial.print(mqttClient.state());
        Serial.println(" try again in 5 seconds");
      #endif
      delay(5000);
    }
  }

  bool connected = mqttClient.connected();

  if (connected)
  {
    mqttClient.subscribe(topicAirTemp);
  }
  
  return connected;
}

void mqttCallback(char* topic, byte* payload, unsigned int length)
{
  if (String(topic) == topicAirTemp)
  {
    pv_last = pv;
    pv = String((char*)payload).toFloat();
  }
}

void setup(void)
{
	Serial.begin(115200);

  if (!wifiConnect())
  {
    Serial.println("Fatal: could't connect to WiFi");
    return;
  }
  Serial.println("WiFi initialized");

  mqttClient.setCallback(mqttCallback);

  if (!mqttConnect())
  {
    Serial.println("Fatal: could't connect to MQTT");
    return;
  }
  Serial.println("MQTT initialized");

	server.on("/", handleRoot);
	server.on("/status", handleGetStatus);

	server.begin();
	Serial.println("HTTP server started");

	ts = millis();
	ot.begin(handleInterrupt);
}

void loop(void)
{
	new_ts = millis();
	if (new_ts - ts > 1000)
  {		
		//Set/Get Boiler Status
		bool enableCentralHeating = true;
		bool enableHotWater = true;
		bool enableCooling = false;
		unsigned long response = ot.setBoilerStatus(enableCentralHeating, enableHotWater, enableCooling);
		OpenThermResponseStatus responseStatus = ot.getLastResponseStatus();
		
    if (responseStatus != OpenThermResponseStatus::SUCCESS)
    {
			Serial.println("Error: Invalid boiler response " + String(response, HEX));
		}

		dt = (new_ts - ts) / 1000.0;
		ts = new_ts;

		if (responseStatus == OpenThermResponseStatus::SUCCESS)
    {
      if (pv_last > invalidTempValue)
      {
        op = max(30.f, pid(sp, pv, pv_last, ierr, dt));
        Serial.printf("OP = %f\n", op);
        ot.setBoilerTemperature(op);
      }

      isCentralHeatingActive = ot.isCentralHeatingActive(response);
      isHotWaterEnabled = ot.isHotWaterActive(response);
      isFlameOn = ot.isFlameOn(response);
      boilerTemperature = ot.getBoilerTemperature();

      mqttClient.publish(topicBoilerHeating, isCentralHeatingActive ? "1" : "0");
      mqttClient.publish(topicBoilerWater, isHotWaterEnabled ? "1" : "0");
      mqttClient.publish(topicBoilerFlame, isFlameOn ? "1" : "0");
      mqttClient.publish(topicBoilerTemp, String(boilerTemperature, 1).c_str());
		}
	}

  if (mqttConnect())
  {
    mqttClient.loop();  // handle mqtt 
  }

	server.handleClient();  // handle http requests
}

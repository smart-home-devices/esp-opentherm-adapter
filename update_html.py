import subprocess


result = subprocess.run(['yarn', 'minify'], shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')

html = result[result.find('<!doctypehtml>'):result.find('Done in')]
escaped_html = html.replace('"', '\\"')

with open('opentherm-adapter.ino', 'r', encoding='utf-8') as fd:
    lines = fd.readlines()

for idx, line in enumerate(lines):
    if line.startswith('const char HTTP_HTML[] PROGMEM ='):
        lines[idx] = f'const char HTTP_HTML[] PROGMEM = "{escaped_html}";\n'
        break

with open('opentherm-adapter.ino', 'w', encoding='utf-8') as fd:
    fd.writelines(lines)
